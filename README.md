## Synopsis

con2fbmap is a utility for swapping an fbtft-based device as the console.  This build is a metabuild that wraps the public release of con2fbmap into an opkg for release.

## Build

### Cross compile and packaging

To cross compile the application use the cross.sh script.  To get a simple usage statement for this script use the following command.

    ./cross.sh -?

Use of the cross compile script requires specifying three components.

1. The path to the cross compiler toolchain directory.
1. The path to the PiBox root file system staging directory.
1. The path to the opkg tools on the host system.

The first two components can be found under the respective build trees of the PiBox Development Platform build.  They may also be distributed as part of the packaged PiBox Development Platform.  Either way, the argument for these options is a directory path.

The last component requires installation of the opkg tools on the host.  These can be built from the PiBox Development Platform using the following target.

    make opkg
    make opkg-install

Additional information for building can be found in the INSTALL file.

## Installation

con2fbmap is packaged in an opkg format.  After cross compiling look in the opkg directory for an .opk file.  This is the file to be installed on the target system.

To install the package on the target, copy the file to the system or the SD card that is used to boo the system.  The use the following command.

    opkg install <path to file>/con2fbmap_1.0_arm.opk

If the package has been installed previously, use the following command to reinstall it.

    opkg --force-reinstall install <path to file>/con2fbmap_1.0_arm.opk

## Contributors

To get involved with PiBox, contact the project administrator:
Michael J. Hammel <mjhammel@graphics-muse.org>

## License

0BSD

